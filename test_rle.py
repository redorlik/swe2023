from hypothesis import given,settings
from hypothesis.strategies import text,characters
from rle import encode,WrongInputType,decode
import pytest

def test_encode():
    assert encode('kkk') == '3k'

def test_y():
    assert encode('yyy') == '3y'

def test_more():
    assert encode('xxxyyy') == '3x3y'

def test_more2():
    assert encode(27*'x'+26*'y') == '26x27y'

def test_more2():
    assert encode(27*'x'+26*'y'+30*' ') == '27x26y30 '

def test_digits():
    assert encode(27*'0'+26*'1'+30*'7') == '270261307'

def test_empty():
    assert encode('') == ''

def test_except():
    with pytest.raises(WrongInputType):
        encode(10)

def test_decode():
    assert decode('3k') == 'kkk'

def test_decode1():
    assert decode('3k4y') == 'kkkyyyy'

def test_decode2():
    assert decode('3k4.') == 'kkk....'

def test_decode3():
    assert decode('30k50y') == 30*'k'+50*'y'

def test_encode_decode():
    s = 'kkkk'
    assert decode(encode(s)) == s

def test_encode_decode():
    assert decode(encode('kkkkyyyyttttoooooooooo')) == \
      'kkkkyyyyttttoooooooooo'

def test_encode_decode():
    assert decode(encode('☺️'*5+'👌'*7)) == '☺️'*5+'👌'*7

def test_o():
    assert decode(encode('᭐')) == '᭐'

@given(text(alphabet=characters(blacklist_characters='0123456789')))
@settings(max_examples=2000)
def test_hypo(x):
    y = encode(x)
    #print(x)
    assert decode(y) == x


