
class WrongInputType(Exception):
    pass

def encode(a):
    if type(a) != str:
        raise WrongInputType()
    if not a:
        return a
    res = []
    old = a[0]
    i = 1
    for c in a[1:]:
        if c == old:
            i +=1
        else:
            res.append((i,old))
            old = c
            i = 1
    else:
        res.append((i,old))
    return ''.join([f"{x[0]}{x[1]}" for x in res])

def decode(msg):
    res = []
    i = 0
    for ch in msg:
        if ch in '0123456789':
            i = 10*i + int(ch)
        else:
            res.append(i*ch)
            i = 0
    return ''.join(res)

